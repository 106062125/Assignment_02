var tutorState = {
    preload: function(){
        game.load.image('background', 'assets/tutorial.jpg');
    },
    create: function(){
        game.add.image(0,0,'background');
    },
    update: function(){
        
        var eKey = game.input.keyboard.addKey(Phaser.Keyboard.E);
        eKey.onDown.add(this.easy, this);
        var hKey = game.input.keyboard.addKey(Phaser.Keyboard.H);
        hKey.onDown.add(this.hard, this);
    },
    easy: function(){
        game.global.flag=0;
        game.global.score=0;
        game.global.ufo_num=0;
        game.global.tag=0;
        game.state.start('easy');
    },
    hard: function(){
        game.global.flag=0;
        game.global.score=0;
        game.global.ufo_num=0;
        game.global.tag=0;
        game.state.start('hard');
    }
}
game.state.add('tutor', tutorState);