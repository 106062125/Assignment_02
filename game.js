var easyState = {
    preload: function() {
        game.load.image('background', 'assets/background.jpg');
        game.load.spritesheet('life','assets/life.png',30,30);
        game.load.spritesheet('player','assets/fighter.png',100,100);
        game.load.spritesheet('enemy','assets/ufo.png',100,60);
        game.load.spritesheet('bullet','assets/bullet.png',15,40);
        game.load.spritesheet('bomb_left','assets/bomb_left.png',18,40);
        game.load.spritesheet('bomb','assets/bomb.png',30,67);
        game.load.spritesheet('attack','assets/enemypower.png',10,35);
        game.load.spritesheet('heart','assets/heart.png',50,69);
        game.load.spritesheet('boss','assets/boss.png',150,150);
        game.load.image('pixel', 'assets/pixel.png');
        game.load.audio('shoot', 'assets/shoot.mp3');
    },
    create: function() {
        shoot = game.add.audio('shoot');
        //background
        this.background = game.add.tileSprite(0, 0, 650, 500, 'background');
        this.player = game.add.sprite(325, 430, 'player');
        this.player.anchor.setTo(0.5,0.5);
        //player
        game.physics.arcade.enable(this.player);
        this.player.speed = 130;
        this.player.body.collideWorldBounds = true;
        //this.player.body.setSize(20, 20, 0, -5);
        this.player.life=5;
        this.player.animations.add('hurt', [0, 1], 8, true);
        
        //score
        game.global.score = 0;
        this.scoreLabel = game.add.text(20, 10, 'Score: 0', { font: '20px Arial', fill: '#ffffff' });
        //enemy(ufo)
        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(100, 'enemy');
        this.enemyPool.setAll('anchor.x', 0.5);
        this.enemyPool.setAll('anchor.y', 0.5);
        this.enemyPool.setAll('outOfBoundsKill', true);
        this.enemyPool.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.enemyPool);
        //player's bullet
        this.bulletPool = game.add.group();
        this.bulletPool.enableBody = true;
        this.bulletPool.createMultiple(1000, 'bullet');
        this.bulletPool.setAll('anchor.x', 0.5);
        this.bulletPool.setAll('anchor.y', 0.5);
        game.physics.arcade.enable(this.bulletPool);
        //player's bomb
        this.bombPool = game.add.group();
        this.bombPool.enableBody = true;
        this.bombPool.createMultiple(10, 'bomb');
        this.bombPool.setAll('anchor.x', 0.5);
        this.bombPool.setAll('anchor.y', 0.5);
        this.bombPool.number=2;
        game.physics.arcade.enable(this.bombPool);
        //heart to get life back
        this.heartPool = game.add.group();
        this.heartPool.enableBody = true;
        this.heartPool.createMultiple(100, 'heart');
        this.heartPool.setAll('anchor.x', 0.5);
        this.heartPool.setAll('anchor.y', 0.5);
        this.heartPool.setAll('outOfBoundsKill', true);
        this.heartPool.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.heartPool);
        //super power left
        this.spPool = game.add.group();
        this.spPool.enableBody = true;
        this.spPool.createMultiple(100, 'bomb_left');
        this.spPool.setAll('anchor.x', 0.5);
        this.spPool.setAll('anchor.y', 0.5);
        game.physics.arcade.enable(this.spPool);
        //enemy attack
        this.attackPool = game.add.group();
        this.attackPool.enableBody = true;
        this.attackPool.createMultiple(1000, 'attack');
        this.attackPool.setAll('anchor.x', 0.5);
        this.attackPool.setAll('anchor.y', 0.5);
        game.physics.arcade.enable(this.attackPool);
        //Life
        this.lifePool = game.add.group();
        this.lifePool.enableBody = true;
        this.lifePool.createMultiple(100, 'life');
        this.lifePool.setAll('anchor.x', 0.5);
        this.lifePool.setAll('anchor.y', 0.5);
        game.physics.arcade.enable(this.lifePool);
        //emitter
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 0;
        //some time delays
        this.nextEnemyAt = 0;
        this.nextShotAt = 0;
        this.shotDelay = 200;
        this.nextAttack = 1000;
        this.attackDelay = game.rnd.integerInRange(2000, 5000);
        this.enemyDelay = 2200;
        this.stoptime = 0;
        this.stopdelay = 2000;
        this.hurttime = 0;
        this.hurtdelay = 1000;
        this.gothittime = 0;
        this.gothitdelay = 1000;
        this.nextHeart = 0;
        this.heartDelay = game.rnd.integerInRange(5000, 15000);
        this.nextbomb=0;
        this.bombdelay=500;
        this.gotbombtime=0;
        this.gotbombDelay=1000;
        this.bosshittime = 0;
        this.bosshitdelay=1000;
        this.bossmovedelay=500;
        this.bossmovetime=0;
        this.cursor = game.input.keyboard.createCursorKeys();
        //life image
        for(var i=0,pos=620;i<this.player.life;i++,pos-=35){
            var life = this.lifePool.getFirstExists(false);
            life.reset(pos, 20);
        }
        for(var j=0,pos=605;j<2;j++,pos+=25){
            var sp = this.spPool.getFirstExists(false);
            sp.reset(pos, 60);
        }
        pause_label = game.add.text(10, 460, 'Pause', { font: '24px Arial', fill: '#fff' });
        pause_label.inputEnabled = true;
        pause_label.events.onInputUp.add(function () {
            game.paused = true;
        });
        game.input.onDown.add(this.onpause, this);
    },
    onpause:function (event){
        if(game.paused){
            game.paused = false;
        }
    },
    update: function(){    
        this.background.tilePosition.y += 2;
        if(game.time.now>=this.stoptime) this.player.animations.stop(null,true);
        if(game.time.now>=this.bosshittime && game.global.flag==1) this.boss.animations.stop(null,true);
        game.physics.arcade.overlap(this.enemyPool, this.bombPool,this.bombenemy, null, this);
        game.physics.arcade.overlap(this.bulletPool, this.enemyPool,this.Killenemy, null, this);
        if(game.global.flag==1){
            game.physics.arcade.overlap(this.boss, this.bombPool,this.bombboss, null, this);
            game.physics.arcade.overlap(this.bulletPool, this.boss,this.Killboss, null, this);
            if(game.time.now>=this.hurttime) {
                game.physics.arcade.overlap(this.player, this.boss,this.playerCollide, null, this);
            }
            this.moveboss();
        }    
        game.physics.arcade.overlap(this.heartPool, this.player,this.getheart, null, this);
        if(game.time.now>=this.hurttime) {
            game.physics.arcade.overlap(this.player, this.enemyPool,this.playerCollide, null, this);
        }
        
        if(game.time.now>=this.gothittime) game.physics.arcade.overlap(this.attackPool, this.player,this.playerHit, null, this);
        
        if(game.global.ufo_num>20 && game.global.flag==0){
            this.addboss();
        }
        else if(game.global.flag==0){
            this.addenemy();
            this.enemyshoot();
        }
        else{
            this.bossshoot();
        }
        
        this.giveheart();
        this.moveplayer();
        
        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.shoot();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.X) && this.bombPool.number>0) {
            
            this.shootbomb();
        }
        
    },
    addboss: function(){
        game.global.flag=1;
        this.boss = game.add.sprite(320, -50, 'boss');
        game.add.tween(this.boss).to({y:100}, 1000).start();
        this.boss.anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.boss);
        this.boss.body.velocity.y= 0;
        this.boss.body.collideWorldBounds = true;
        this.boss.animations.add('hit', [0, 1], 6, true);
        //this.boss.body.setSize(20, 20, 0, -5);
        this.boss.life=100;
        this.boss.movespeed=2;
    },
    addenemy:function(){
        
        if (this.nextEnemyAt<game.time.now && this.enemyPool.countDead()>0) {
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.enemyPool.getFirstExists(false);
            enemy.reset(game.rnd.integerInRange(50, 560), 0);
            enemy.body.velocity.y = game.rnd.integerInRange(30, 60);
            game.global.ufo_num++;
        }
         
    },
    moveplayer:function(){
        this.player.body.velocity.x=0;
        this.player.body.velocity.y=0;
        if(this.cursor.left.isDown){
            this.player.body.velocity.x=-this.player.speed;
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x=this.player.speed;
        }
        if(this.cursor.up.isDown){
            this.player.body.velocity.y=-this.player.speed;
        }
        else if(this.cursor.down.isDown){
            this.player.body.velocity.y=this.player.speed;
        }
        
    },
    moveboss: function(){
        this.boss.body.x+=this.boss.movespeed;
        if(this.boss.body.x>=500) this.boss.movespeed=-2;
        else if(this.boss.body.x<=5) this.boss.movespeed=2;
    },
    shoot:function(){
          if (!this.player.alive || this.nextShotAt>game.time.now) {
            return;
          }
          if (this.bulletPool.countDead()==0) {
            return;
          }
          this.nextShotAt = game.time.now + this.shotDelay;
          shoot.play();
          var bullet = this.bulletPool.getFirstExists(false);
          bullet.reset(this.player.x, this.player.y-50);
          bullet.body.velocity.y = -200;
    },
    shootbomb: function(){
        if (!this.player.alive || this.nextbomb>game.time.now) {
            return;
        }
        if (this.bombPool.countDead()==0) {
        return;
        }
        this.nextbomb = game.time.now + this.bombdelay;
        shoot.play();
        var bomb = this.bombPool.getFirstExists(false);
        bomb.reset(this.player.x, this.player.y-50);
        bomb.body.velocity.y = -200;
        this.bombPool.number--;
        var bomb_left = this.spPool.getFirstAlive(false);
        bomb_left.kill();
    },
    Killenemy: function(bullet,enemy){
        bullet.kill();
        enemy.kill();
        this.emitter.x = enemy.x;
        this.emitter.y = enemy.y;
        this.emitter.start(true, 800, null, 15);
        game.global.score += 5;
        this.scoreLabel.text = 'Score: ' + game.global.score;
    },
    bombenemy: function(bomb,enemy){
        bomb.kill();
        enemy.kill();
        this.emitter.x = enemy.x;
        this.emitter.y = enemy.y;
        this.emitter.start(true, 800, null, 15);
        game.global.score += 15;
        this.scoreLabel.text = 'Score: ' + game.global.score;
    },
    Killboss: function(boss,bullet){
        bullet.kill();
        boss.life-=5;
        game.global.score += 5;
        boss.animations.play('hit');
        this.bosshittime=game.time.now+this.bosshitdelay;
        this.scoreLabel.text = 'Score: ' + game.global.score;
        if(boss.life<=0) {
            game.global.tag=1;
            this.playerend();
        }
    },
    bombboss: function(boss,bomb){
        bomb.kill();
        boss.life-=15;
        game.global.score += 15;
        boss.animations.play('hit');
        this.bosshittime=game.time.now+this.bosshitdelay;
        this.scoreLabel.text = 'Score: ' + game.global.score;
        if(boss.life<=0) {
            game.global.tag=1;
            this.playerend();
        }
    },
    enemyshoot: function(){
        
        if (this.nextAttack>game.time.now) {
            return;
        }
        var attackenemy=this.enemyPool.getRandomExists();
        if(attackenemy==undefined) return;
        this.nextAttack = game.time.now + this.attackDelay;
        var attack1 = this.attackPool.getFirstExists(false);
        attack1.reset(attackenemy.x, attackenemy.y+20);
        attack1.body.velocity.y = +200;
        var attack2 = this.attackPool.getFirstExists(false);
        attack2.reset(attackenemy.x, attackenemy.y+20);
        attack2.body.velocity.x = +100;
        attack2.body.velocity.y = +200;
        var attack3 = this.attackPool.getFirstExists(false);
        attack3.reset(attackenemy.x, attackenemy.y+20);
        attack3.body.velocity.x = -100;
        attack3.body.velocity.y = +200;
        
        
    },
    bossshoot: function(){
        if (this.nextAttack>game.time.now) {
            return;
        }
        this.nextAttack = game.time.now + this.attackDelay;
        var attack1 = this.attackPool.getFirstExists(false);
        attack1.reset(this.boss.x, this.boss.y+20);
        attack1.body.velocity.x = +200;
        attack1.body.velocity.y = +200;
        var attack2 = this.attackPool.getFirstExists(false);
        attack2.reset(this.boss.x, this.boss.y+20);
        attack2.body.velocity.x = +100;
        attack2.body.velocity.y = +200;
        var attack3 = this.attackPool.getFirstExists(false);
        attack3.reset(this.boss.x, this.boss.y+20);
        attack3.body.velocity.x = -100;
        attack3.body.velocity.y = +200;
        var attack4 = this.attackPool.getFirstExists(false);
        attack4.reset(this.boss.x, this.boss.y+20);
        attack4.body.velocity.x = -200;
        attack4.body.velocity.y = +200;
        var attack5 = this.attackPool.getFirstExists(false);
        attack5.reset(this.boss.x, this.boss.y+20);
        attack5.body.velocity.y = +200;
    },
    playerHit: function(player,attack){
        this.emitter.x = attack.x;
        this.emitter.y = attack.y;
        this.emitter.start(true, 800, null, 15);
        attack.kill();
        this.stoptime=game.time.now+this.stopdelay;
        this.gothitttime = game.time.now + this.gothitdelay;
        this.hurttime = game.time.now + this.hurtdelay;
        
        player.animations.play('hurt');
        if(player.life>1){
            player.life--;
            var life = this.lifePool.getFirstAlive(false);
            var tmp  = this.lifePool.getFurthestFrom(life);
            tmp.kill();
        }
        else {
            var life = this.lifePool.getFirstAlive(false);
            if(life == null) return;
            life.kill();
            player.kill();
            this.playerend();
        }
        
    },
    playerCollide: function(player, enemy){
        
        this.stoptime=game.time.now+this.stopdelay;
        this.hurttime = game.time.now + this.hurtdelay;
        player.animations.play('hurt');
        if(player.life>1){
            player.life--;
            var life = this.lifePool.getFirstAlive(false);
            var tmp  = this.lifePool.getFurthestFrom(life);
            tmp.kill();
        }
        else {
            var life = this.lifePool.getFirstAlive(false);
            if(life == null) return;
            life.kill();
            player.kill();
            this.playerend();
        }
    },
    playerend: function(){
        game.state.start('end');
    },
    giveheart: function(){
        if (this.nextHeart<game.time.now ) {
            this.nextHeart = game.time.now + this.heartDelay;
            var heart = this.heartPool.getFirstExists(false);
            heart.reset(game.rnd.integerInRange(50, 560), 0);
            heart.body.velocity.y = 50;
        } 
    },
    getheart: function(player,heart){
        heart.kill();
        if(player.life>=5) return;
        if(player.life>1){
            var life = this.lifePool.getFirstAlive(false);
            var tmp  = this.lifePool.getFurthestFrom(life);
        }
        else {
            var tmp = this.lifePool.getFirstAlive(false);
        }
        var newlife = this.lifePool.getFirstExists(false);
        newlife.reset(tmp.x-35, 20);
        this.player.life++;
    }
};
game.global = { score: 0, tag: 0 };
game.global = { ufo_num: 0 ,flag: 0 ,level: 0};
game.state.add('easy', easyState);
