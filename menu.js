var menuState = {
    preload: function(){
        game.load.image('background', 'assets/menu_back.jpg');
        game.load.image('title', 'assets/title.png');
        game.load.image('alien', 'assets/alien.png');
        game.load.image('astronaut', 'assets/astronaut.png');
        game.load.image('fighter', 'assets/fighter1.png');
        game.load.image('ufo', 'assets/ufo1.png');
        game.load.image('planet', 'assets/planet.png');
        game.load.image('start', 'assets/start.png');
        game.load.image('press', 'assets/press.png');
        game.load.audio('music', 'assets/music.mp3');
        game.load.image('up', 'assets/up.png');
        game.load.image('down', 'assets/down.png');
        
    },
    create: function(){
        
        game.add.image(0,0,'background');
        volume_up = game.add.image(20, 10, 'up');
        volume_up.inputEnabled = true;
        volume_up .events.onInputUp.add(this.volumeup);
        volume_down = game.add.image(75, 10, 'down');
        volume_down.inputEnabled = true;
        volume_down.events.onInputUp.add(this.volumedown);
        
        
        var title = game.add.image(310, -50,'title');
        title.anchor.setTo(0.5, 0.5);
        game.add.tween(title).to({y:120}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        
        var start = game.add.image(310, 285,'start');
        start.anchor.setTo(0.5, 0.5);
        var press = game.add.image(155, 250,'press');
        press.anchor.setTo(0.5, 0.5);

        var alien = game.add.image(230, 550,'alien');
        title.anchor.setTo(0.5, 0.5);
        game.add.tween(alien).to({y:375}, 1000).easing(Phaser.Easing.Bounce.Out).start();

        var astronaut = game.add.image(112, 550,'astronaut');
        astronaut.anchor.setTo(0.5, 0.5);
        game.add.tween(astronaut).to({y:435}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        
        var fighter = game.add.image(560, 550,'fighter');
        fighter.anchor.setTo(0.5, 0.5);
        game.add.tween(fighter).to({y:426}, 1000).easing(Phaser.Easing.Bounce.Out).start();

        var ufo = game.add.image(417, 550,'ufo');
        ufo.anchor.setTo(0.5, 0.5);
        game.add.tween(ufo).to({y:437}, 1000).easing(Phaser.Easing.Bounce.Out).start();

        var planet = game.add.image(700, 90,'planet');
        planet.anchor.setTo(0.5, 0.5);
        game.add.tween(planet).to({x:550}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);
    },
    update: function(){
        console.log(game.global.music.volume);
    },
    volumeup: function(){
        if(game.global.music.volume>=0.91) game.global.music.volume=1;
        else game.global.music.volume+=0.1;
    },
    volumedown: function(){
        if(game.global.music.volume<=0.01) game.global.music.volume=0;
        else game.global.music.volume-=0.1;
    },
    start: function(){
        game.state.start('tutor');
    }
}
game.state.add('menu', menuState);
game.global = { music };