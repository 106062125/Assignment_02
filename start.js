var startState = {
    preload: function (){
        game.load.image('background', 'assets/menu_back.jpg');
        
        game.load.audio('music', 'assets/music.mp3');
    },
    create: function(){
        game.global.music = game.add.audio('music');
        game.global.music.loop = true; 
        game.global.music.play();
        game.add.image(0,0,'background');
        var barGreen,barYellow,maxWidth,tween;
        var loadingLabel = game.add.text(game.width/2, 200, 'Loading...', { font: '30px AR CHRISTY', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        barGreen = game.add.graphics(150,300);
        barGreen.beginFill(0xEAF516);
        barGreen.drawRect(0,0,350,30);

        barYellow = game.add.graphics(150,300);
        barYellow.beginFill(0x4BFAF7);
        barYellow.drawRect(0,0,350,30);
        
        maxWidth = 350;
        barYellow.width=0;
        
        tween = game.add.tween(barYellow);
        tween.to({width:maxWidth},1500);
        this.time=game.time.now+1600;
        tween.start();
        tween.onComplete.add(this.change,this);
        
    },
    change: function(){
        game.state.start('menu');
    }
}

var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
game.state.add('start', startState);
game.state.start('start');