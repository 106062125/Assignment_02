# Assignment_02

# Software Studio 2019 Spring Assignment 2
## Notice

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 分別有兩關不同難度做選擇，hard level多了兩個boss要攻擊。玩家有兩次使用砲彈的機會，砲彈的殺傷力較高。
2. Animations : boss和玩家被攻擊都會出現閃爍
3. Particle Systems : ufo被子彈打到便會以particle形式消失，同樣的玩家被敵人子彈攻擊，敵人子彈也會以此方式消失。
4. Sound effects : 背景音樂及玩家射擊音效，menu能調整背景音樂音量。
5. UI : 遊戲畫面顯示玩家當前生命值以及砲彈數量，畫面左上方顯示分數，點左下方PAUSE即可暫停，menu能調整背景音樂音量。

# Bonus Functions Description : 
1. Enhanced items : 遇到愛心瓶會增加生命值。
2. BOSS : 生命值較高須多次攻擊才會死亡，EASY最終有一個boss，HARD中間有兩個最後有一個。