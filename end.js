var endState = {
    preload: function(){
        game.load.image('background', 'assets/end_back.jpg');
    },
    create: function(){
        game.add.image(0,0,'background');
        if(game.global.tag==1) var finalLabel = game.add.text(210, 100,"You Win!!", { font: '60px AR CHRISTY', fill: '#ffffff' });
        else var finalLabel = game.add.text(210, 100,"You Lose!", { font: '60px AR CHRISTY', fill: '#ffffff' });
        this.scoreLabel = game.add.text(270, 200, 'Score: '+game.global.score, { font: '30px AR CHRISTY', fill: '#ffffff' });
    },
    update: function(){
        var AKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
        AKey.onDown.add(this.playagain, this);
        var MKey = game.input.keyboard.addKey(Phaser.Keyboard.M);
        MKey.onDown.add(this.back2menu, this);
    },
    playagain: function(){
        game.state.start('tutor');
    },
    back2menu: function(){
        game.state.start('menu');
    }
}
game.state.add('end', endState);